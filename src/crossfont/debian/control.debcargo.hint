Source: rust-crossfont
Section: rust
Priority: optional
Build-Depends: debhelper (>= 11),
 dh-cargo (>= 18),
 cargo:native <!nocheck>,
 rustc:native <!nocheck>,
 libstd-rust-dev <!nocheck>,
 librust-euclid-0.20+default-dev <!nocheck>,
 librust-foreign-types-0.5+default-dev <!nocheck>,
 librust-freetype-rs-0.26+default-dev <!nocheck>,
 librust-libc-0.2+default-dev <!nocheck>,
 librust-log-0.4+default-dev <!nocheck>,
 librust-pkg-config-0.3+default-dev <!nocheck>,
 librust-servo-fontconfig-0.5+default-dev (>= 0.5.1-~~) <!nocheck>
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Fabio Rafael da Rosa <fdr@fabiodarosa.org>
Standards-Version: 4.4.1
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/crossfont]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/crossfont

Package: librust-crossfont-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-euclid-0.20+default-dev,
 librust-foreign-types-0.5+default-dev,
 librust-freetype-rs-0.26+default-dev,
 librust-libc-0.2+default-dev,
 librust-log-0.4+default-dev,
 librust-pkg-config-0.3+default-dev,
 librust-servo-fontconfig-0.5+default-dev (>= 0.5.1-~~)
Suggests:
 librust-crossfont+force-system-fontconfig-dev (= ${binary:Version})
Provides:
 librust-crossfont+default-dev (= ${binary:Version}),
 librust-crossfont-0-dev (= ${binary:Version}),
 librust-crossfont-0+default-dev (= ${binary:Version}),
 librust-crossfont-0.1-dev (= ${binary:Version}),
 librust-crossfont-0.1+default-dev (= ${binary:Version}),
 librust-crossfont-0.1.1-dev (= ${binary:Version}),
 librust-crossfont-0.1.1+default-dev (= ${binary:Version})
Description: Cross platform native font loading and rasterization - Rust source code
 This package contains the source for the Rust crossfont crate, packaged by
 debcargo for use with cargo and dh-cargo.

Package: librust-crossfont+force-system-fontconfig-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-crossfont-dev (= ${binary:Version}),
 librust-servo-fontconfig-0.5+force-system-lib-dev (>= 0.5.1-~~)
Provides:
 librust-crossfont-0+force-system-fontconfig-dev (= ${binary:Version}),
 librust-crossfont-0.1+force-system-fontconfig-dev (= ${binary:Version}),
 librust-crossfont-0.1.1+force-system-fontconfig-dev (= ${binary:Version})
Description: Cross platform native font loading and rasterization - feature "force_system_fontconfig"
 This metapackage enables feature "force_system_fontconfig" for the Rust
 crossfont crate, by pulling in any additional dependencies needed by that
 feature.
